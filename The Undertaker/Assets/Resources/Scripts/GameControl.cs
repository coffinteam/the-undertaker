﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameControl : MonoBehaviour
{
    //Name of variables that defines player atributes during battles
    //TODO: Change name of skills to be descriptive about what skill is doing (and add more than one)
    public int maxHealth;
    public int maxMana;
    public bool firstSkill = false;

    public static GameControl control;

	// Use this for initialization
	void Awake ()
    {
        if (control == null)
        {
            DontDestroyOnLoad(gameObject);
            control = this;
        }
        else if (control != this)
        {
            Destroy(gameObject);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //Serializes and saves player atributes to file
    public void Save(String name)
    {
        BinaryFormatter bf = new BinaryFormatter();

        //If there is no "saves" directory it creates it
        //After that it creates save file with given name
        if (!Directory.Exists(Application.persistentDataPath + "/saves"))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/saves");
        }
        FileStream file = File.Create(Application.persistentDataPath + "/saves/" + name + ".sav");

        //Copy ALL data from GameControl to PlayerData Object
        PlayerData data = new PlayerData();
        data.maxHealth = maxHealth;
        data.maxMana = maxMana;
        data.firstSkill = firstSkill;
        bf.Serialize(file, data);

        file.Close();
    }

    //Watch out Load function takes argument with extension 
    public void Load(String nameWithExtension)
    {
        if (File.Exists(Application.persistentDataPath + "/saves/" + nameWithExtension))
        {
            BinaryFormatter bf = new BinaryFormatter();

            FileStream file = File.Open(Application.persistentDataPath + "/saves/" + nameWithExtension, FileMode.Open);
            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();
            //Like before edit this atributes when they arrive :P
            maxHealth = data.maxHealth;
            maxMana = data.maxMana;
            firstSkill = data.firstSkill;
        }
    }
}

//Class that is used to serialize and save player atributes. 
//So it has to contain all player variables from GameControl class
[Serializable]
class PlayerData
{
    public int maxHealth;
    public int maxMana;
    public bool firstSkill; 
}