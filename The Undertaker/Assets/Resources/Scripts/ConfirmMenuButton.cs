﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class ConfirmMenuButton : Button
{
    public void onClick()
    {
        SceneManager.LoadScene(0);
    }
}