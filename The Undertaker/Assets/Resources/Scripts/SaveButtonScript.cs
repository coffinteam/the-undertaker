﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System;


public class SaveButtonScript : Button
{
    public GameObject SavePanel;

    public void onClick()
    {
        GameObject.Find("StoryContents").SetActive(false);
        SavePanel.SetActive(true);
        EventSystem.current.SetSelectedGameObject(GameObject.Find("BackButton"));
    }
}


