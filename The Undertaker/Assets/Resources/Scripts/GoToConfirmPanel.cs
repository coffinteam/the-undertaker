﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class GoToConfirmPanel : Button
{
    public GameObject currentObject;
    public GameObject quitMenu;

    public void onClick()
    {
        currentObject.SetActive(false);
        quitMenu.SetActive(true);
        EventSystem.current.SetSelectedGameObject(GameObject.Find("BackButtonQ"));
    }
}
