﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuLoadButton : Button
{
    public GameObject buttonPrefab;
    public GameObject loadButtons;

    public void onClick()
    {
        //Deactivate Main Menu Buttons and activate load menu
        GameObject.Find("MenuButtons").SetActive(false);
        loadButtons.SetActive(true);

        //Check if there are files in save directory if so creat buttons for each save
        string[] listaPLikow = Directory.GetFiles(Application.persistentDataPath + "/saves", "*.sav");
        for (int i = 0; i < listaPLikow.Length; i++)
        {
            GameObject newButton = Instantiate<GameObject>(buttonPrefab);
            newButton.transform.SetParent(loadButtons.transform);
            newButton.GetComponentInChildren<Text>().text = Path.GetFileName(listaPLikow[i]);
            newButton.transform.localScale = new Vector3(1, 1, 1);
            newButton.transform.localPosition = new Vector3(0, 0 - 50 * (i + 1), 0);
        }
        //Sets Back Button as current for keyboard control
        EventSystem.current.SetSelectedGameObject(GameObject.Find("BackButtonL"));
    }
}
