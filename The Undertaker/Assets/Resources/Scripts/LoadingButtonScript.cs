﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadingButtonScript : MonoBehaviour
{
    public Button loadButton;

    //Check if this formula is good
    public void onClick()
    {
        GameControl.control.Load(loadButton.GetComponentInChildren<Text>().text);
    }	
}