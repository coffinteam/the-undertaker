﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class MainMenuGUI : MonoBehaviour
{
    public EventSystem eventSystem;
    public GameObject currentSelected;

    // Initializing first selected button for keyboard control
    void Start ()
    {
        currentSelected = eventSystem.firstSelectedGameObject;
    }
	
	//Checking if only one button is selected all the time
	void Update ()
    {
        if (eventSystem.currentSelectedGameObject != currentSelected)
        {
            if (eventSystem.currentSelectedGameObject == null)
            {
                eventSystem.SetSelectedGameObject(currentSelected);
            }
            else
            {
                currentSelected = eventSystem.currentSelectedGameObject;
            }
        }
    }
}
