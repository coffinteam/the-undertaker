﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System;

public class NewGameButtonScript : Button
{

	public void onClick()
    {
        SceneManager.LoadScene(1);
    }
}
