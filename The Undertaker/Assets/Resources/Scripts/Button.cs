﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public abstract class Button : MonoBehaviour, IPointerEnterHandler
{
    public void OnPointerEnter(PointerEventData eventData)
    {
        EventSystem.current.SetSelectedGameObject(gameObject);
    }
}

