﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System;


public class CreatorsButtonScript : Button
{
    public GameObject credits;

    public void onClick()
    {
        GameObject.Find("MenuButtons").SetActive(false);
        credits.SetActive(true);
        EventSystem.current.SetSelectedGameObject(GameObject.Find("BackButtonC"));
    }
}
