﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System;

public class MenuBackButton : Button
{
    public GameObject nextObject;
    public GameObject currentObject;

    public void onClick()
    {
        currentObject.SetActive(false);
        nextObject.SetActive(true);
        EventSystem.current.SetSelectedGameObject(GameObject.Find("NewGameButton"));
    }
}
